
PROGRAM _INIT
	(* Insert code here *)
	CtrlAxis01.Enable := TRUE;
	CtrlAxis01.MpLink := ADR(gAxisBasic01Link);
	CtrlAxis01.Parameters := ADR(CtrlAxis01Param);
	CtrlAxis01.Axis := ADR(gAxis01);
	
	CtrlAxis02.Enable := TRUE;
	CtrlAxis02.MpLink := ADR(gAxisBasic02Link);
	CtrlAxis02.Parameters := ADR(CtrlAxis02Param);
	CtrlAxis02.Axis := ADR(gAxis02);

	CtrlAxis01Param.Home.Mode := mpAXIS_HOME_MODE_RESTORE_POS;

END_PROGRAM

PROGRAM _CYCLIC
 
	
	(********** vol�n� FB **********)
	CtrlAxis01();
	CtrlAxis02();
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

