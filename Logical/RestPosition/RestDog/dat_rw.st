(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: dat_rw
 * File: dat_rw.st
 * Author: Bernecker + Rainer
 * Created: January 27, 2010
 ********************************************************************
 * Implementation of program dat_rw
 ********************************************************************)

(*************init program**********************)
PROGRAM _INIT

	Status.InitEndlessPosition.Busy := 0;
	Status.InitEndlessPosition.Error := 0;
	Status.InitEndlessPosition.Done := 0;
	
	
	Status.CheckEndlessPosition.Busy := 0;
	Status.CheckEndlessPosition.Error := 0;
	Status.CheckEndlessPosition.Done := 0;
	Status.CheckEndlessPosition.InvalidParamFound := 0;
	Status.CheckEndlessPosition.RestoreParamDone := 0;
	
	(********** homing param CHECK **********)
	gHoming.Axis01.FB_Check.Axis := ADR(gAxis01);
	gHoming.Axis01.FB_Check.DataAddress := ADR(gHomingParam.Axis01.EndlessPositionData);
		
	(********** homing param INIT **********)
	(* This function block is used to transfer the address of the permanent variable for saving and restoring the axis position to the ACP10_MC library.
	The FUNCTION block must be called after each restart OF the target system before the homing procedure. This can be done in the initialization subroutine.  *)
	gHoming.Axis01.FB_Init.Axis := ADR(gAxis01);
	gHoming.Axis01.FB_Init.DataAddress := ADR(gHomingParam.Axis01.EndlessPositionData);
	
END_PROGRAM

(*************cyclic program********************)
PROGRAM _CYCLIC


	(********** Commands **********)
	
	IF Referuj THEN
		Referuj := 0;
		Referuj_case := 10;
	END_IF;
	
	
	
	(******************************* homing param INIT ************************************)
	(* Init mus� b�t zavol�n po ka�d�m restartu - mus� b�t provedeno pred homingem *)
//	CASE InitEndlessPosition_case OF
//		0:
//			(* zmena informac� o stavu Initu *)
//			Status.InitEndlessPosition.Busy := 1;
//			Status.InitEndlessPosition.Error := 0;
//			Status.InitEndlessPosition.Done := 0;
//			
//		(* automaticky hned po restartu proved init *)
//			gHoming.Axis01.FB_Init.Execute := TRUE ;
//			InitEndlessPosition_case := 10;
//		
//		
//		10:
//			IF gHoming.Axis01.FB_Init.Done = TRUE AND gHoming.Axis01.FB_Init.Error = FALSE THEN
//				gHoming.Axis01.FB_Init.Execute := FALSE ;
//				InitEndlessPosition_case := 100;
//				
//				(* zmena informac� o stavu Initu *)
//				Status.InitEndlessPosition.Busy := 0;
//				Status.InitEndlessPosition.Error := 0;
//				Status.InitEndlessPosition.Done := 1;
//				
//			END_IF;
//		
//		
//		100: (* done and wait *)
//		
//		255: (* error *)
//		
//
//	END_CASE;
	
	
	
	
	(********** Po zapnut� zkontrolov�n� zda jsou hodnoty v permanentn� pameti ulo�eny spr�vne a bez erroru **********)
	CASE Validation_RestorePos_case OF
		0: (* hned jakmile PLC nabootuje se koukni jestli hodnoty jsou OK  *)
			gHoming.Axis01.FB_Check.Execute := TRUE ;
			Validation_RestorePos_case := 10;
		
			(* zmena informac� o stavu Check *)
			Status.CheckEndlessPosition.Busy := 1;
//			Status.CheckEndlessPosition.Error := 0;
//			Status.CheckEndlessPosition.Done := 0;
//			Status.CheckEndlessPosition.InvalidParamFound := 0;
//			Status.CheckEndlessPosition.RestoreParamDone := 0;
		
		10: 
			IF gHoming.Axis01.FB_Check.Done = TRUE THEN
				Validation_RestorePos_case := 11;
			END_IF;

		11:
			(* data jsou OK a nen� potreba je prehr�vat  *)
			IF gHoming.Axis01.FB_Check.Done = TRUE AND gHoming.Axis01.FB_Check.DataValid = TRUE THEN
				Validation_RestorePos_case := 100;
				gHoming.Axis01.FB_Check.Execute := FALSE ;
				(* zmena informac� o stavu Check *)
				Status.CheckEndlessPosition.Busy := 0;
				Status.CheckEndlessPosition.Done := 1;	
				(* pokud byli data predt�m po�kozena , potvrd �e jsi je vpor�dku obnovil *)
				IF Status.CheckEndlessPosition.InvalidParamFound = 1 THEN
					Status.CheckEndlessPosition.RestoreParamDone := 1;
				END_IF;
				
			END_IF;
		
			(* data jsou po�kozena a je potreba je obnovit z Data Objektu  *)
			IF gHoming.Axis01.FB_Check.Done = TRUE AND gHoming.Axis01.FB_Check.DataValid = FALSE THEN
				Validation_RestorePos_case := 20;
				gHoming.Axis01.FB_Check.Execute := FALSE ;
				(* zmena informac� o stavu Check *)
				Status.CheckEndlessPosition.Busy := 1;
				Status.CheckEndlessPosition.InvalidParamFound := 1;
			END_IF;
		
		
		20: (* data jsou po�kozena *)
			(* vycti data z objektu *)
			RwDatObj.Commands.read := TRUE;
			Validation_RestorePos_case := 22;
		
		22: 
			(* a� je nacten� z objektu hotov� prekop�ruj ho do Permanentn� pameti *)
			IF RwDatObj.step = DOB_WAIT THEN
				(* Axis 01 *)
				gHomingParam.Axis01.EndlessPositionData[0].MTPhase := RwDatObj.read_data.Axis01.MTPhase0;
				gHomingParam.Axis01.EndlessPositionData[0].MTDiffInteger := RwDatObj.read_data.Axis01.MTDiffInteger0;
				gHomingParam.Axis01.EndlessPositionData[0].MTDiffFract := RwDatObj.read_data.Axis01.MTDiffFract0;
				gHomingParam.Axis01.EndlessPositionData[0].RefOffset := RwDatObj.read_data.Axis01.RefOffset0;
				gHomingParam.Axis01.EndlessPositionData[0].Checksum := RwDatObj.read_data.Axis01.Checksum0;
				gHomingParam.Axis01.EndlessPositionData[0].MTPhase := RwDatObj.read_data.Axis01.MTPhase0;
				
				gHomingParam.Axis01.EndlessPositionData[1].MTPhase := RwDatObj.read_data.Axis01.MTPhase1;
				gHomingParam.Axis01.EndlessPositionData[1].MTDiffInteger := RwDatObj.read_data.Axis01.MTDiffInteger1;
				gHomingParam.Axis01.EndlessPositionData[1].MTDiffFract := RwDatObj.read_data.Axis01.MTDiffFract1;
				gHomingParam.Axis01.EndlessPositionData[1].RefOffset := RwDatObj.read_data.Axis01.RefOffset1;
				gHomingParam.Axis01.EndlessPositionData[1].Checksum := RwDatObj.read_data.Axis01.Checksum1;
				gHomingParam.Axis01.EndlessPositionData[1].MTPhase := RwDatObj.read_data.Axis01.MTPhase1;
			
				Validation_RestorePos_case := 24;	
			END_IF;
		
		
		
		
		
		24:
		(* chyba by mela b�t opravena a zkus�m znova check *)
		Validation_RestorePos_case := 0;
		
		
		100:
		(* done and wait *)

	END_CASE;
	
	
	
	(******************************* Referovan� ************************************)
	
	CASE Referuj_case OF
		0: (* standstill *)
			
		10:
			IF CtrlAxis01.Info.ReadyToPowerOn THEN
				CtrlAxis01.Power := TRUE;
				Referuj_case := 11;
			END_IF;
		11:
			IF CtrlAxis01.PowerOn THEN
				gHoming.Axis01.FB_Init.Execute := TRUE;
				Referuj_case := 20;
			END_IF;
		
		20: 
			IF gHoming.Axis01.FB_Init.Done = TRUE AND gHoming.Axis01.FB_Init.Error = FALSE THEN
				gHoming.Axis01.FB_Init.Execute := FALSE;
				CtrlAxis01Param.Home.Mode := mpAXIS_HOME_MODE_RESTORE_POS;
				CtrlAxis01.Home := TRUE;
				Referuj_case := 21;
			END_IF;
		
		21:
			IF CtrlAxis01.IsHomed = TRUE AND CtrlAxis01.Error = FALSE THEN
				CtrlAxis01.Home := FALSE;
				Referuj_case := 30;
			END_IF;
		
		30:(* zreferov�no *)	
			CtrlAxis01.MoveAbsolute := TRUE; 	// jdi to polohy 0
			Referuj_case := 31;
		
		31: 
			IF CtrlAxis01.InPosition = TRUE THEN
				CtrlAxis01.MoveAbsolute := FALSE;
				Referuj_case := 40;
			END_IF;
		
		40:
			Referuj_case := 0;		
	END_CASE;
		
	(*************************************************** FILE handling ********************************************************)
	CASE RwDatObj.step OF
		
		DOB_WAIT:	(*--- wait for command*)
		
			(*Command for reading the content of the data object*)
			IF RwDatObj.Commands.read = TRUE THEN
				RwDatObj.step := DOB_INFO;
   			END_IF			
			
			(*Command for writing data into the data object*)
			IF RwDatObj.Commands.write = TRUE THEN
				RwDatObj.step := DOB_INFO;
   			END_IF			
		
		DOB_INFO:	(*--- aquire info about the data object*)
		
			(*Parameters for DatObjInfo()*)
			RwDatObj.DatObjInfo_0.enable := TRUE;
			RwDatObj.DatObjInfo_0.pName := ADR('RestPos');						(*name of the data object*)
   			
			RwDatObj.DatObjInfo_0();											(*call function block DatObjInfo()*)		
	
			IF RwDatObj.DatObjInfo_0.status = 0 THEN
				IF RwDatObj.Commands.read = TRUE THEN							(*aquiring information successfull --> next step*)
					RwDatObj.step := DOB_READ;
				ELSE
					RwDatObj.step := DOB_WRITE;
				END_IF
				
			ELSIF RwDatObj.DatObjInfo_0.status = BUSY THEN
				RwDatObj.step := DOB_INFO;										(*function block not finished yet --> call again*)
   			
			ELSE
				RwDatObj.step := DOB_ERROR;										(*function block retrned errorcode --> error handling*)
		
			END_IF
		
		DOB_READ:	(*--- read content of the data object*)
		
			RwDatObj.DatObjRead_0.enable := TRUE;
			RwDatObj.DatObjRead_0.ident := RwDatObj.DatObjInfo_0.ident;
			RwDatObj.DatObjRead_0.Offset := 0;
																				(*limit data size to smalles value*)
			sizeObj := MIN (RwDatObj.DatObjInfo_0.len, SIZEOF(RwDatObj.read_data));
			
			RwDatObj.DatObjRead_0.len :=  sizeObj ;
			RwDatObj.DatObjRead_0.pDestination := ADR(RwDatObj.read_data);		(*destination adress for read data*)
		
			RwDatObj.DatObjRead_0();											(*call function block DatObjRead()*)		
			
			IF RwDatObj.DatObjRead_0.status = 0 THEN
				RwDatObj.Commands.read := FALSE;								(*disable write command*)
				RwDatObj.step := DOB_WAIT;										(*read operation successful --> return to wait step*)
				
			ELSIF RwDatObj.DatObjRead_0.status = BUSY THEN
				RwDatObj.step := DOB_READ;										(*function block not finished yet --> call again*)
				
   			ELSE
				RwDatObj.step := DOB_ERROR;										(*function block retrned errorcode --> error handling*)
		
			END_IF	
		
		DOB_WRITE:	(*--- write data into the data object*)
			RwDatObj.DatObjWrite_0.enable := TRUE;
			RwDatObj.DatObjWrite_0.ident := RwDatObj.DatObjInfo_0.ident;
			RwDatObj.DatObjWrite_0.Offset := 0;
																				(*limit data size to smalles value*)			
			RwDatObj.DatObjWrite_0.len := MIN (RwDatObj.DatObjInfo_0.len, SIZEOF(RwDatObj.write_data));
			RwDatObj.DatObjWrite_0.pSource := ADR(RwDatObj.write_data);			(*source adress of write data*)
		
			RwDatObj.DatObjWrite_0();											(*call function block DatObjRead()*)		
			
			IF RwDatObj.DatObjWrite_0.status = 0 THEN
				RwDatObj.Commands.write := FALSE;								(*disable write command*)
				RwDatObj.step := DOB_WAIT;										(*read operation successful --> return to wait step*)
				
			ELSIF RwDatObj.DatObjWrite_0.status = BUSY THEN
				RwDatObj.step := DOB_WRITE;										(*function block not finished yet --> call again*)
				
   			ELSE
				RwDatObj.step := DOB_ERROR;										(*function block retrned errorcode --> error handling*)
		
			END_IF
					
		DOB_ERROR:	(*--- general error handling*)
			;(*to do: implement your error handling here*)

	END_CASE
		
	
	(******************************* Ukladej strukturu ka�d�ch 30 sec ************************************)

	CASE Save_New_RestorePos_case OF
		0:
		
			IF Validation_RestorePos_case = 100 AND CtrlAxis01.PowerOn = TRUE THEN
		
				(* task 100 ms *)
				Timer := Timer + 100 ;
	
				IF Timer > 30000 THEN	
					IF RwDatObj.step = DOB_WAIT	 THEN
						//	RwDatObj.Commands.write := TRUE;
						Save_New_RestorePos_case := 10;
						Timer := 0;
					END_IF;
				END_IF;	
			END_IF;
		

		10:
				
			gHoming.Axis01.FB_Check.Execute := TRUE ;
			IF gHoming.Axis01.FB_Check.Done = TRUE THEN
				Save_New_RestorePos_case := 20;
			END_IF;

		20:
			(* data jsou OK a mohu zapsat *)
			IF gHoming.Axis01.FB_Check.Done = TRUE AND gHoming.Axis01.FB_Check.DataValid = TRUE THEN
				Save_New_RestorePos_case := 40;
				gHoming.Axis01.FB_Check.Execute := FALSE ;
				
				RwDatObj.write_data.Axis01.MTPhase0 := gHomingParam.Axis01.EndlessPositionData[0].MTPhase;
				RwDatObj.write_data.Axis01.MTDiffInteger0 := gHomingParam.Axis01.EndlessPositionData[0].MTDiffInteger;
				RwDatObj.write_data.Axis01.MTDiffFract0 := gHomingParam.Axis01.EndlessPositionData[0].MTDiffFract;
				RwDatObj.write_data.Axis01.RefOffset0  := gHomingParam.Axis01.EndlessPositionData[0].RefOffset;
				RwDatObj.write_data.Axis01.Checksum0  := gHomingParam.Axis01.EndlessPositionData[0].Checksum;
				RwDatObj.write_data.Axis01.MTPhase0  := gHomingParam.Axis01.EndlessPositionData[0].MTPhase;
				
				RwDatObj.write_data.Axis01.MTPhase1 := gHomingParam.Axis01.EndlessPositionData[1].MTPhase;
				RwDatObj.write_data.Axis01.MTDiffInteger1 := gHomingParam.Axis01.EndlessPositionData[1].MTDiffInteger;
				RwDatObj.write_data.Axis01.MTDiffFract1 := gHomingParam.Axis01.EndlessPositionData[1].MTDiffFract;
				RwDatObj.write_data.Axis01.RefOffset1  := gHomingParam.Axis01.EndlessPositionData[1].RefOffset;
				RwDatObj.write_data.Axis01.Checksum1  := gHomingParam.Axis01.EndlessPositionData[1].Checksum;
				RwDatObj.write_data.Axis01.MTPhase1  := gHomingParam.Axis01.EndlessPositionData[1].MTPhase;
				
				RwDatObj.Commands.write := TRUE;		
			END_IF;
		
			(* data jsou po�kozena a neprepisuji *)
			IF gHoming.Axis01.FB_Check.Done = TRUE AND gHoming.Axis01.FB_Check.DataValid = FALSE THEN
				Validation_RestorePos_case := 0;
				gHoming.Axis01.FB_Check.Execute := FALSE ;
				Timer := 0;
			END_IF;
		
		

		
		
		40:
			(* zaps�no a opet skoc na zac�tek *)
			IF RwDatObj.step = DOB_WAIT	 THEN
				Save_New_RestorePos_case := 0;
				Timer := 0;
			END_IF;

	END_CASE;
	
	
	
	
	
	(********** vol�n� FB **********)
	gHoming.Axis01.FB_Init();
	gHoming.Axis01.FB_Check();	
	
	
END_PROGRAM
