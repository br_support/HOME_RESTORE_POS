(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: dat_rw
 * File: Sender.typ
 * Author: Bernecker + Rainer
 * Created: January 25, 2010
 ********************************************************************
 * Local data types of program dat_rw
 ********************************************************************)
(*Enumeration for Statemachine*)

TYPE
	enDATRW_STEPS : 
		(
		DOB_WAIT := 0, (*Defines the wait step*)
		DOB_INFO, (*Defines Step for DatObjInfo()*)
		DOB_READ, (*Defines Step for DatObjRead()*)
		DOB_WRITE, (*Defines Step for DatObjWrite()*)
		DOB_ERROR := 255 (*Defines Step for Errorhandling*)
		);
END_TYPE

(*Command data type*)

TYPE
	command_typ : 	STRUCT 
		read : BOOL; (*command: read data from data object 'dat_org'*)
		write : BOOL; (*command: wirte data to data object 'dat_org'*)
	END_STRUCT;
END_TYPE

(*Sender type*)

TYPE
	dat_org_typ : 	STRUCT 
		Axis01 : EndlessPositionData_struc;
	END_STRUCT;
	EndlessPositionData_struc : 	STRUCT 
		MTPhase0 : DINT;
		MTDiffInteger0 : DINT;
		MTDiffFract0 : DINT;
		RefOffset0 : DINT;
		Checksum0 : UDINT;
		MTPhase1 : DINT;
		MTDiffInteger1 : DINT;
		MTDiffFract1 : DINT;
		RefOffset1 : DINT;
		Checksum1 : UDINT;
	END_STRUCT;
END_TYPE

(*Main structure containing all instances and local variables*)

TYPE
	Dat_rw_typ : 	STRUCT 
		step : enDATRW_STEPS; (*Step of the Statemachine*)
		Commands : command_typ; (*Commandinterface*)
		read_data : dat_org_typ; (*read data from data objects*)
		write_data : dat_org_typ; (*write data for data object*)
		DatObjInfo_0 : DatObjInfo; (*Functionblock DatObjInfo()*)
		DatObjRead_0 : DatObjRead; (*Functionblock DatObjRead()*)
		DatObjWrite_0 : DatObjWrite; (*Functionblock DatObjWrite()*)
	END_STRUCT;
	StatusHomig_type : 	STRUCT 
		InitEndlessPosition : StatusHomigInit_type;
		CheckEndlessPosition : StatusHomigCheck_type;
	END_STRUCT;
	StatusHomigInit_type : 	STRUCT 
		Error : BOOL;
		Busy : BOOL;
		Done : BOOL;
	END_STRUCT;
	StatusHomigCheck_type : 	STRUCT 
		Error : BOOL;
		Busy : BOOL;
		Done : BOOL;
		InvalidParamFound : BOOL;
		RestoreParamDone : BOOL;
	END_STRUCT;
END_TYPE
