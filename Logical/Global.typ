
TYPE
	gHomingParam_type : 	STRUCT 
		Axis01 : MC_ENDLESS_POSITION;
		Axis02 : MC_ENDLESS_POSITION;
		Axis03 : MC_ENDLESS_POSITION;
	END_STRUCT;
	gHoming_type : 	STRUCT 
		Axis01 : gHomingAxis_type;
		Axis02 : gHomingAxis_type;
		Axis03 : gHomingAxis_type;
	END_STRUCT;
	gHomingAxis_type : 	STRUCT 
		FB_Init : MC_BR_InitEndlessPosition;
		FB_Check : MC_BR_CheckEndlessPosition;
	END_STRUCT;
END_TYPE
